using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ScheduleApi.Data;
using ScheduleApi.Models;

[Route("v1/Rules")]
public class RulesController : Controller
{
    [HttpGet]
    [Route("")]
    [Authorize]
    public async Task<ActionResult<List<Rules>>> Get([FromServices] DataContext context)
    {
        DateTime latestDate = context.Rules.Max(f => f.InsertDate).Date;
        var rules = await context.Rules.Include(x => x.Period)
                                       .Where(x => x.InsertDate.Date >= latestDate)
                                       .Include(x => x.DeliveryPeriod).AsNoTracking().ToListAsync();
        return rules;
    }

    [HttpGet]
    [Route("{id:int}")]
    [Authorize]
    public async Task<ActionResult<Rules>> GetById([FromServices] DataContext context, int id)
    {
        DateTime latestDate = context.Rules.Max(f => f.InsertDate).Date;
        var rules = await context.Rules.Include(x => x.Period)
                                       .Where(x => x.InsertDate.Date >= latestDate)
                                       .Include(x => x.DeliveryPeriod).AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
        return rules;
    }

    [HttpGet]
    [Route("periods/{id:int}")]
    [Authorize]
    public async Task<ActionResult<List<Rules>>> GetByPeriod([FromServices] DataContext context, int id)
    {
        DateTime latestDate = context.Rules.Max(f => f.InsertDate).Date;
        var rules = await context.Rules.Include(x => x.Period)
                                       .Where(x => x.Period.Id == id)
                                       .Where(x => x.InsertDate.Date >= latestDate)
                                       .Include(x => x.DeliveryPeriod).AsNoTracking().ToListAsync();
        return rules;
    }

    [HttpPost]
    [Route("")]
    [Authorize]
    public async Task<ActionResult<Rules>> Post(
        [FromServices] DataContext context,
        [FromBody] Rules model)
    {
        if (ModelState.IsValid)
        {
            context.Rules.Add(model);
            await context.SaveChangesAsync();
            return model;
        }
        else
        {
            return BadRequest(ModelState);
        }
    }

    [HttpDelete]
    [Route("{id:int}")]
    [Authorize]
    public async Task<ActionResult<Rules>> Delete(
            [FromServices] DataContext context,
            int id)
    {
        var rule = await context.Rules.FirstOrDefaultAsync(x => x.Id == id);
        if (rule == null)
            return NotFound(new { message = "Regra não encontrada" });

        try
        {
            context.Rules.Remove(rule);
            await context.SaveChangesAsync();
            return Ok(new { message = "Regra removida com sucesso" });
        }
        catch (Exception)
        {
            return BadRequest(new { message = "Não foi possível remover a regra" });
        }
    }

    [HttpPost]
    [Route("periodsIds")]
    [Authorize]
    public async Task<ActionResult<List<Rules>>> GetRules([FromServices] DataContext context, [FromBody] List<int> ids)
    {
        DateTime latestDate = context.Rules.Max(f => f.LastUpdate).Date;
        var rules = await context.Rules.Include(x => x.Period)
                                       .Where(x => ids.Any(y => y == x.Period.Id))
                                       .Where(x => x.InsertDate.Date >= latestDate)
                                       .Include(x => x.DeliveryPeriod).AsNoTracking().ToListAsync();
        return rules;
    }
}