using System;
using System.ComponentModel.DataAnnotations;

namespace ScheduleApi.Models
{
  public class Rules
  {
    [Key]
    public int Id { get; set; }

    public int PeriodId { get; set; }

    public Period Period { get; set; }

    public int DeliveryPeriodId { get; set; }
    
    public Period DeliveryPeriod { get; set; }

    public int AdditionalDay { get; set; }

    public int OrderQuantity { get; set; }
    
    public DateTime InsertDate { get; set; }

    public DateTime LastUpdate { get; set; }

    public StatusEnum.Estatus Status { get; set; }
  }
}