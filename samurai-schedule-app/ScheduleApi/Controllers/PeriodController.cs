using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ScheduleApi.Data;
using ScheduleApi.Models;

[Route("v1/Periods")]
public class PeriodController : ControllerBase
{
    [HttpGet]
    [Route("Region/{region:int}")]
    [Authorize]
    public async Task<ActionResult<List<Period>>> GetPeriods(
        [FromServices] DataContext context, int region)
    {
        var periods = await context.Periods.AsNoTracking().Where(x => x.RegionId == region).Include(x => x.Region).ToListAsync();
        return Ok(periods);
    }

    [HttpGet]
    [Route("")]
    [Authorize]
    public async Task<ActionResult<List<Period>>> Get(
    [FromServices] DataContext context)
    {
        var periods = await context.Periods.AsNoTracking().Include(x => x.Region).ToListAsync();
        return Ok(periods);
    }

    [HttpGet]
    [Route("{id:int}")]
    [Authorize]
    public async Task<ActionResult<Period>> GetById(
      int id,
      [FromServices] DataContext context)
    {
        var periods = await context.Periods.AsNoTracking().Include(x => x.Region).FirstOrDefaultAsync(x => x.Id == id);
        return Ok(periods);
    }

    [HttpPost]
    [Route("")]
    [Authorize]
    public async Task<ActionResult<List<Period>>> Post(
      [FromBody] Period model,
      [FromServices] DataContext context)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);

        try
        {
            context.Periods.Add(model);
            await context.SaveChangesAsync();
            return Ok(model);
        }
        catch
        {
            return BadRequest(new { message = "Não foi possível criar o período" });
        }
    }

    [HttpPut]
    [Route("{id:int}")]
    [Authorize]
    public async Task<ActionResult<List<Period>>> Put(
      int id,
      [FromBody] Period model,
      [FromServices] DataContext context)
    {
        var period = await context.Periods.FirstOrDefaultAsync(x => x.Id == id);
        if (period == null)
            return NotFound(new { message = "Período não encontrado" });

        try
        {
            period.LastUpdate = DateTime.Now;
            context.Entry(period).State = EntityState.Modified;
            await context.SaveChangesAsync();
            return Ok(period);
        }
        catch (DbUpdateConcurrencyException)
        {
            return BadRequest(new { message = "Este registro já foi atualizado" });
        }
        catch (System.Exception)
        {
            return BadRequest(new { message = "Não foi possível atualizar o período" });
        }
    }

    [HttpDelete]
    [Route("{id:int}")]
    [Authorize]
    public async Task<ActionResult<Period>> Delete(
              [FromServices] DataContext context,
              int id)
    {
        var period = await context.Periods.FirstOrDefaultAsync(x => x.Id == id);
        if (period == null)
            return NotFound(new { message = "Período não encontrada" });

        try
        {
            context.Rules.RemoveRange(context.Rules.Where(r => r.PeriodId == period.Id || r.DeliveryPeriodId == period.Id));

            context.Periods.Remove(period);
            await context.SaveChangesAsync();
            return Ok(new { message = "Período removida com sucesso" });
        }
        catch (Exception)
        {
            return BadRequest(new { message = "Não foi possível remover o período" });
        }
    }
}