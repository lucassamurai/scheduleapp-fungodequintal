using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ScheduleApi.Data;
using ScheduleApi.Models;

[Route("v1/Ranges")]
public class ShippingRangeController : Controller
{
    [HttpGet]
    [Route("cep/{cep:int}")]
    [Authorize]
    public async Task<ActionResult<List<ShippingRange>>> GetRegion([FromServices] DataContext context, string cep)
    {
        return await context.Ranges.Include(x => x.Region).Where(x => Convert.ToInt32(x.StartingRange) <= Convert.ToInt32(cep) &&
                                                                      Convert.ToInt32(x.FinalRange) >= Convert.ToInt32(cep)).AsNoTracking().ToListAsync();
    }

    [HttpGet]
    [Route("")]
    [Authorize]
    public async Task<ActionResult<List<ShippingRange>>> Get([FromServices] DataContext context)
    {
        var ranges = await context.Ranges.Include(x => x.Region).AsNoTracking().ToListAsync();
        return ranges;
    }

    [HttpGet]
    [Route("{id:int}")]
    [Authorize]
    public async Task<ActionResult<ShippingRange>> GetById([FromServices] DataContext context, int id)
    {
        var ranges = await context.Ranges.Include(x => x.Region).AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
        return ranges;
    }

    [HttpGet]
    [Route("regions/{id:int}")]
    [Authorize]
    public async Task<ActionResult<List<ShippingRange>>> GetByRegion([FromServices] DataContext context, int id)
    {
        var ranges = await context.Ranges.Include(x => x.Region).Where(x => x.Region.Id == id).AsNoTracking().ToListAsync();
        return ranges;
    }

    [HttpPost]
    [Route("")]
    [Authorize]
    public async Task<ActionResult<ShippingRange>> Post(
        [FromServices] DataContext context,
        [FromBody] ShippingRange model)
    {
        if (ModelState.IsValid)
        {
            context.Ranges.Add(model);
            await context.SaveChangesAsync();
            return model;
        }
        else
        {
            return BadRequest(ModelState);
        }
    }

    [HttpDelete]
    [Route("{id:int}")]
    [Authorize]
    public async Task<ActionResult<ShippingRange>> Delete(
            [FromServices] DataContext context,
            int id)
    {
        var range = await context.Ranges.FirstOrDefaultAsync(x => x.Id == id);
        if (range == null)
            return NotFound(new { message = "Faixa de cep não encontrada" });

        try
        {
            context.Ranges.Remove(range);
            await context.SaveChangesAsync();
            return Ok(new { message = "Faixa de cep removida com sucesso" });
        }
        catch (Exception)
        {
            return BadRequest(new { message = "Não foi possível remover a faixa de cep" });
        }
    }
}