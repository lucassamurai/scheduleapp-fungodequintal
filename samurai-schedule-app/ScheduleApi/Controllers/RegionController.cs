using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ScheduleApi.Data;
using ScheduleApi.Models;

[Route("v1/Regions")]
public class RegionController : ControllerBase
{
  [HttpGet]
  [Route("")]
  [Authorize]
  public async Task<ActionResult<List<Region>>> Get(
    [FromServices]DataContext context)
  {
    var regions = await context.Regions.AsNoTracking().ToListAsync();
    return Ok(regions);
  }

  [HttpGet]
  [Route("{id:int}")]
  [Authorize]
  public async Task<ActionResult<Region>> GetById(
    int id,
    [FromServices]DataContext context)
  {
    var regions = await context.Regions.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
    return Ok(regions);
  }

  [HttpPost]
  [Route("")]
  [Authorize]
  public async Task<ActionResult<List<Region>>> Post(
    [FromBody]Region model,
    [FromServices]DataContext context)
  {
    if (!ModelState.IsValid)
      return BadRequest(ModelState);

    try
    {
      context.Regions.Add(model);
      await context.SaveChangesAsync();
      return Ok(model);
    }
    catch
    {
      return BadRequest(new { message = "Não foi possível criar a região" });
    }
  }

  [HttpPut]
  [Route("{id:int}")]
  [Authorize]
  public async Task<ActionResult<List<Region>>> Put(
    int id,
    [FromBody]Region model,
    [FromServices]DataContext context)
  {
    var region = await context.Regions.FirstOrDefaultAsync(x => x.Id == id);
    if (region == null)
      return NotFound(new { message = "Região não encontrada" });

    try
    {
      region.LastUpdate = DateTime.Now;
      region.Status = model.Status;
      context.Entry(region).State = EntityState.Modified;
      await context.SaveChangesAsync();
      return Ok(region);
    }
    catch (DbUpdateConcurrencyException)
    {
      return BadRequest(new { message = "Este registro já foi atualizado" });
    }
    catch (System.Exception)
    {
      return BadRequest(new { message = "Não foi possível atualizar a região" });
    }
  }

  [HttpDelete]
  [Route("{id:int}")]
  [Authorize]
  public async Task<ActionResult<Region>> Delete(
            [FromServices] DataContext context,
            int id)
  {
    var region = await context.Regions.FirstOrDefaultAsync(x => x.Id == id);
    if (region == null)
      return NotFound(new { message = "Região não encontrada" });

    try
    {
      context.Holidays.RemoveRange(context.Holidays.Where(r => r.RegionId == region.Id));
      context.Regions.Remove(region);
      await context.SaveChangesAsync();
      return Ok(new {message = "Região removida com sucesso"});
    }
    catch (Exception)
    {
      return BadRequest(new { message = "Não foi possível remover a região" });
    }
  }
}