using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using RestSharp;
using System.Net;
using ScheduleApi.Models;
using ScheduleApi.Services;
using Google.Rpc;
using System;

[Route("v1/Orders")]
public class OrderController : ControllerBase
{
    private readonly OrderService _order;
    public OrderController(OrderService order) 
    {
        _order = order;
    }
  [HttpGet]
  [Route("")]
  [Authorize]
  public async Task<ActionResult> Get([FromServices] ScheduleApi.Services.OrderService order)
  {
    var all = await order.GetOrderCount("a-beladodia.myshopify.com", "aditional_info_shipschedule");
    return new JsonResult(all.Select(_ => new { schedule = _.Note, count = _.Count }));
  }

    [Produces("application/json")]
    [HttpPut]
    [Route("")]
    [Authorize]
    public async Task<ActionResult> Put(
        [FromBody] Order order
        )
    {
        if (order is null)
        {
            return BadRequest("Objeto Order Nulo: " + order);
        }
        try
        {
            var resultSendRequest = await _order.UpdateOrder(order);
            return Ok();

        }
        catch (Exception e)
        {
            return BadRequest(e);
        }
    }
}