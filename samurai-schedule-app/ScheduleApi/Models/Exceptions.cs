using System;
using System.ComponentModel.DataAnnotations;

namespace ScheduleApi.Models
{
  public class Exceptions
  {
    [Key]
    public int Id { get; set; }

    public string DayOfWeek { get; set; }
    
    public int PeriodId { get; set; }

    public Period Period { get; set; }

    public DateTime InsertDate { get; set; }

    public DateTime LastUpdate { get; set; }

    public StatusEnum.Estatus Status { get; set; }
  }
}