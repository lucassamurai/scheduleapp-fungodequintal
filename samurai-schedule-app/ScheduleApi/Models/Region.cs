using System;
using System.ComponentModel.DataAnnotations;

namespace ScheduleApi.Models
{
  public class Region
  {
    [Key]
    public int Id { get; set; }

    public string Title { get; set; }

    public DateTime InsertDate { get; set; }

    public DateTime LastUpdate { get; set; }

    public StatusEnum.Estatus Status { get; set; }
  }
}