namespace ScheduleApi
{
  public static class StatusEnum
  {
      public enum Estatus
      {
        Active = 1,
        Inactive = 2,
        Deleted = 3
      }
  }
}