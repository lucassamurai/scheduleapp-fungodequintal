using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScheduleApi.Models
{
  public class Holiday
  {
    [Key]
    public int Id { get; set; }

    public string Name { get; set; }

    public string Date { get; set; }

    public int NationalHoliday { get; set; }

    [ForeignKey("Holiday")]
    public int? RegionId { get; set; }

    public Region Region { get; set; }

    public DateTime InsertDate { get; set; }

    public DateTime LastUpdate { get; set; }

    public StatusEnum.Estatus Status { get; set; }
  }
}