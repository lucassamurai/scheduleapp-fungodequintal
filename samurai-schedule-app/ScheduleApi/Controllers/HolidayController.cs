using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ScheduleApi.Data;
using ScheduleApi.Models;

[Route("v1/Holidays")]
public class HolidayController : ControllerBase
{
  [HttpGet]
  [Route("")]
  [Authorize]
  public async Task<ActionResult<List<Holiday>>> Get(
    [FromServices]DataContext context)
  {
    var holidays = await context.Holidays.AsNoTracking().Include(x => x.Region).ToListAsync();
    return Ok(holidays);
  }

  [HttpGet]
  [Route("{id:int}")]
  [Authorize]
  public async Task<ActionResult<Holiday>> GetById(
    int id,
    [FromServices]DataContext context)
  {
    var holidays = await context.Holidays.AsNoTracking().Include(x => x.Region).FirstOrDefaultAsync(x => x.Id == id);
    return Ok(holidays);
  }

  [HttpPost]
  [Route("")]
  [Authorize]
  public async Task<ActionResult<List<Holiday>>> Post(
    [FromBody]Holiday model,
    [FromServices]DataContext context)
  {
    if (!ModelState.IsValid)
      return BadRequest(ModelState);

    try
    {
      context.Holidays.Add(model);
      await context.SaveChangesAsync();
      return Ok(model);
    }
    catch
    {
      return BadRequest(new { message = "Não foi possível criar o Feriado" });
    }
  }

  [HttpPut]
  [Route("{id:int}")]
  [Authorize]
  public async Task<ActionResult<List<Holiday>>> Put(
    int id,
    [FromBody]Holiday model,
    [FromServices]DataContext context)
  {
    var holiday = await context.Holidays.FirstOrDefaultAsync(x => x.Id == id);
    if (holiday == null)
      return NotFound(new { message = "Feriado não encontrado" });

    try
    {
      holiday.LastUpdate = DateTime.Now;
      holiday.Status = model.Status;
      context.Entry(holiday).State = EntityState.Modified;
      await context.SaveChangesAsync();
      return Ok(holiday);
    }
    catch (DbUpdateConcurrencyException)
    {
      return BadRequest(new { message = "Este registro já foi atualizado" });
    }
    catch (System.Exception)
    {
      return BadRequest(new { message = "Não foi possível atualizar o feriado" });
    }
  }

  [HttpDelete]
  [Route("{id:int}")]
  [Authorize]
  public async Task<ActionResult<Holiday>> Delete(
            [FromServices] DataContext context,
            int id)
  {
    var holiday = await context.Holidays.FirstOrDefaultAsync(x => x.Id == id);
    if (holiday == null)
      return NotFound(new { message = "Feriado não encontrada" });

    try
    {
      context.Holidays.Remove(holiday);
      await context.SaveChangesAsync();
      return Ok(new {message = "Feriado removida com sucesso"});
    }
    catch (Exception)
    {
      return BadRequest(new { message = "Não foi possível remover o feriado" });
    }
  }
}