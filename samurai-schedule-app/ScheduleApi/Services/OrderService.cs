﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ScheduleApi.Data;
using ScheduleApi.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Authenticators;
using System.Net;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace ScheduleApi.Services
{
    public class OrderService
    {

        private readonly DataContext Database;
        protected readonly IConfiguration _configuration;

        public OrderService(DataContext context, IConfiguration configuration)
        {
            Database = context;
            _configuration = configuration;

        }

        public async Task<Object> UpdateOrder(Order order) {
            var resultSendRequest = await ExecuteCall(
                $"/admin/api/2021-04/orders/{order.id}.json",
                Method.PUT,
                HttpStatusCode.OK,
                order
            );
            return resultSendRequest;
        }
        public async Task<List<(string Note, long Count)>> GetOrderCount(string myShopifyDomain, string noteName)
        {
            var shop = await Database.ShopifyShops.FirstOrDefaultAsync(x => x.MyshopifyDomain == myShopifyDomain);
            if (shop == null)
                return null;

            string accessToken = shop.AccessToken;
            OrderResult orderResult = null;
            var responseShopify = await GetFromShopify(myShopifyDomain, "orders.json", "?limit=150&fields=financial_status,fulfillment_status,note_attributes", accessToken);
            string nextPage = "";
            do
            {
                nextPage = "";
                if (responseShopify.Headers.ToList().Where(_ => _.Key == "Link").Any())
                {
                    var objectHeader = responseShopify.Headers.ToList().Where(_ => _.Key == "Link").FirstOrDefault().Value.FirstOrDefault();
                    nextPage = getNextUrl(objectHeader);
                }
                if (orderResult == null)
                    orderResult = JsonConvert.DeserializeObject<OrderResult>(await responseShopify.Content.ReadAsStringAsync().ConfigureAwait(false));
                else
                    orderResult.orders.AddRange(JsonConvert.DeserializeObject<OrderResult>(await responseShopify.Content.ReadAsStringAsync().ConfigureAwait(false)).orders);

                if (!string.IsNullOrEmpty(nextPage))
                {
                    responseShopify = await GetFromShopify(myShopifyDomain, "orders.json", new Uri(nextPage).Query, accessToken);
                }
            } while (!string.IsNullOrEmpty(nextPage));

            var orderList = orderResult.orders.Where(_ => (_.financial_status ==  OrderResult.FinancialStatus.paid || _.financial_status == OrderResult.FinancialStatus.pending) && (_.fulfillment_status == null || _.fulfillment_status != OrderResult.OrderFulfillmentStatus.fulfilled));

            var orderFixed = (from order in orderList
                              select new
                              {
                                  note = order.note_attributes?.FirstOrDefault(_ => _.name == noteName)?.value ?? ""
                              }).ToList();

            return orderFixed
                        .GroupBy(l => l.note)
                        .Select(cl => (Note: cl.Key, Count: cl.LongCount()))
                        .ToList();
        }



        private async Task<HttpResponseMessage> GetFromShopify(string shop, string endpoint, string filters, string accessToken)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri($"https://{shop}");
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.Add("X-Shopify-Access-Token", accessToken);

                var response = await httpClient.GetAsync($"/admin/api/2020-01/{endpoint}{filters}").ConfigureAwait(false);

                if (!response.IsSuccessStatusCode)
                    return null;

                return response;
            }
        }

        private string getNextUrl(string header)
        {
            return ParseLinkHeader(header).Where(x => x.Rel == "next").Select(x => x.Url).FirstOrDefault();
        }

        private static Regex _urlRegex = new Regex("<(?<url>.*)>");
        private static Regex _relRegex = new Regex("rel=\"(?<rel>.*)\"");
        private List<(string Rel, string Url)> ParseLinkHeader(string header)
        {
            return header
                        .Split(',')
                        .Select(link =>
                        {
                            var parts = link.Split("; ");
                            return (_relRegex.Match(parts[1]).Groups["rel"].Value, _urlRegex.Match(parts[0]).Groups["url"].Value);
                        }).ToList();
        }

            private async Task<JObject> ExecuteCall(string resource, Method httpMethod, object rootObj,Order order)
        { 
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            var shop = await Database.ShopifyShops.FirstOrDefaultAsync(x => x.MyshopifyDomain == "a-beladodia.myshopify.com");
            if (shop == null)
                return null;            
            string jsonGet = await GetOrderShopify(shop.Name, shop.AccessToken, "?limit=150&fields=financial_status,fulfillment_status,note_attributes", order);
            ShopifyOrder deserializedProduct = null;
            try{deserializedProduct = JsonConvert.DeserializeObject<ShopifyOrder>(jsonGet);}catch (Exception e) {return null;}
            string jsonAtribbutes = "";
            int validadornote = 0;
            int validadoratributo = 0;
            for (int i = 0; i < deserializedProduct.order.note_attributes.Count(); i++) 
            {
                if (deserializedProduct.order.note_attributes[i].name == "aditional_info_shipschedule")
                {
                    validadoratributo = 1;
                    if (deserializedProduct.order.note_attributes[i].value == null || deserializedProduct.order.note_attributes[i].value == " " || deserializedProduct.order.note_attributes[i].value == "-")
                    {    
                        validadornote = 1;
                        return null;
                    }
                    jsonAtribbutes += "{\"name\":\"" + deserializedProduct.order.note_attributes[i].name + "\", \"value\" : \"" + order.NoteAttributes + "\"}";
                }
                else 
                {
                    jsonAtribbutes += "{\"name\":\""+ deserializedProduct.order.note_attributes[i].name + "\", \"value\" : \"" + deserializedProduct.order.note_attributes[i].value + "\"},";
                }
            }
            if(validadoratributo ==0)
                jsonAtribbutes += "{\"name\":\"aditional_info_shipschedule\", \"value\" : \"" + order.NoteAttributes + "\"}";

            if (validadornote == 0)
            {
                var key = "8912847ccb0442d36218a94cb5eb7ca1";
                var pass = "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";
                string jsonField = "{ \"order\": {\"id\":" + order.id + ", \"note_attributes\":[" + jsonAtribbutes + "]} }";
                JObject json = JObject.Parse(jsonField);
                var request = new RestRequest(resource, httpMethod);
                var client = new RestClient(string.Format("https://{0}.myshopify.com", shop.Name));
                client.Authenticator = new HttpBasicAuthenticator(key, pass);
                client.Timeout = 30000;
                request.AddHeader("cache-control", "no-cache");
                request.AddParameter("application/json", json, ParameterType.RequestBody);
                var response = client.Execute(request);

                return JObject.Parse(response.Content);
            }
            return null;
        }

        private async Task<string> GetOrderShopify(string shop, string accesstoken, string filters, Order order)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri($"https://{shop}.myshopify.com");
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.Add("X-Shopify-Access-Token", accesstoken);
                try
                {
                    var response = await httpClient.GetAsync($"/admin/api/2021-04/orders/{order.id}.json");
                    if (!response.IsSuccessStatusCode)
                        return null;

                    return response.Content.ReadAsStringAsync().Result;
                }
                catch (Exception e) { }
                return null;
            }
        }
    }
}
