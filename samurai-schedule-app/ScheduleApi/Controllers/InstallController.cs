﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ScheduleApi.Data;
using ScheduleApi.Models;

namespace ScheduleApi.Controllers
{
    [Route("install")]
    public class InstallController : Controller
    {
        private readonly ILogger<InstallController> _logger;
        private readonly string APP_NAME;
        private readonly string APP_URL;
        private readonly string DASHBOARD_URL;
        private readonly string CLIENT_ID;
        private readonly string CLIENT_SECRET;
        private readonly string SCOPE;
        private readonly string SECRET;

        private readonly DataContext Database;

        public InstallController(ILogger<InstallController> logger, IConfiguration configuration, DataContext context)
        {
            _logger = logger;
            APP_NAME = configuration["AppSettings:Name"];
            APP_URL = configuration["AppSettings:Url"];
            DASHBOARD_URL = configuration["AppSettings:DashboarUrl"];
            CLIENT_ID = configuration["AppSettings:ClientId"];
            CLIENT_SECRET = configuration["AppSettings:ClientSecret"];
            SCOPE = configuration["AppSettings:Scopes"];
            SECRET = configuration["Auth:Secret"];

            Database = context;
        }

        [HttpGet]
        [Route("CreateApp")]
        public async Task<IActionResult> CreateApp(string hmac, string shop, string timestamp)
        {
            var redirectURL = $"{APP_URL}/Install/CreateAppRedirect";

            return Redirect($"https://{shop}/admin/oauth/authorize?client_id={CLIENT_ID}" +
                $"&scope={SCOPE}&redirect_uri={redirectURL}&state=1");
        }

        [HttpGet]
        [Route("CreateAppRedirect")]
        public async Task<IActionResult> CreateAppRedirect(string code, string hmac, string timestamp, string state, string shop)
        {
            try
            {
                var tokenShopify = await GetAccessTokenAsync(shop, CLIENT_ID, CLIENT_SECRET, code).ConfigureAwait(false);

                if (tokenShopify == null)
                    throw new Exception("Error getting access token");

                var content = await GetShopInfoAtShopifyAsync(shop, tokenShopify).ConfigureAwait(false);

                if (content == null)
                    throw new Exception("Error getting store info");

                var shopifyShop = await SaveShopifyShopInfoAsync(shop, code, tokenShopify, content).ConfigureAwait(false);

                return Redirect(DASHBOARD_URL);
            }
            catch (Exception ex)
            {
                _logger.LogError("CreateAppRedirect", ex);
                throw;
            }
        }

        private async Task<AccessTokenShopifyDTO> GetAccessTokenAsync(string shop, string clientId, string clientSecret, string code)
        {
            var json = JsonConvert.SerializeObject(new
            {
                client_id = clientId,
                client_secret = clientSecret,
                code = code
            });

            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri($"https://{shop}");
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await httpClient.PostAsync("/admin/oauth/access_token", new StringContent(json, Encoding.UTF8, "application/json"))
                    .ConfigureAwait(false);

                if (!response.IsSuccessStatusCode)
                    return null;

                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                if (content == null)
                {
                    return null;
                }

                return JsonConvert.DeserializeObject<AccessTokenShopifyDTO>(content);
            }
        }
        private async Task<string> GetShopInfoAtShopifyAsync(string shop, AccessTokenShopifyDTO tokenShopify)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri($"https://{shop}");
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.Add("X-Shopify-Access-Token", tokenShopify.access_token);

                var response = await httpClient.GetAsync("/admin/shop.json").ConfigureAwait(false);

                if (!response.IsSuccessStatusCode)
                    return null;

                return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            }
        }
        private async Task<ShopifyShop> SaveShopifyShopInfoAsync(string shop, string code, AccessTokenShopifyDTO tokenShopify, string content)
        {
            var isNew = false;
            dynamic objectShop = JObject.Parse(content);

            var shopifyShop = await Database.ShopifyShops.FirstOrDefaultAsync(x => x.MyshopifyDomain == shop).ConfigureAwait(false);

            if (shopifyShop == null)
            {
                isNew = true;
                shopifyShop = new ShopifyShop { CreationDate = DateTime.Now };
                Database.ShopifyShops.Add(shopifyShop);
            }

            shopifyShop.Address1 = objectShop.shop.address1;
            shopifyShop.City = objectShop.shop.city;
            shopifyShop.Country = objectShop.shop.country;
            shopifyShop.CountryName = objectShop.shop.country_name;
            shopifyShop.CreationDate = DateTime.Now;
            shopifyShop.Domain = objectShop.shop.domain;
            shopifyShop.Email = objectShop.shop.email;
            shopifyShop.IdShopify = objectShop.shop.id;
            shopifyShop.JsonShop = content;
            shopifyShop.MyshopifyDomain = objectShop.shop.myshopify_domain;
            shopifyShop.Name = objectShop.shop.name;
            shopifyShop.Phone = objectShop.shop.phone;
            shopifyShop.Province = objectShop.shop.province;
            shopifyShop.Zip = objectShop.shop.zip;
            shopifyShop.Code = code;
            shopifyShop.AccessToken = tokenShopify.access_token;
            shopifyShop.UpdateDate = DateTime.Now;

            await Database.SaveChangesAsync().ConfigureAwait(false);

            return shopifyShop;
        }

        public class AccessTokenShopifyDTO
        {
            public string access_token { get; set; }
        }
    }
}